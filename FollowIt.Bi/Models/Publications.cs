namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Publications
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public string Body { get; set; }

        public bool Hot { get; set; }

        public bool Sticky { get; set; }

        public bool HomePage { get; set; }

        public string Tags { get; set; }

        public string Location { get; set; }

        public string ContactPersonName { get; set; }

        public int PublicationType { get; set; }

        public int VacancyType { get; set; }

        public int PublicationStatus { get; set; }

        public DateTime? PublishDate { get; set; }

        public long DemandId { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public string CandidateProfile { get; set; }

        public string WhatYouGet { get; set; }

        public DateTime? DueDate { get; set; }

        public Guid PublicationIdentifier { get; set; }

        public string DemandNumber { get; set; }

        public virtual Demands Demands { get; set; }
    }
}
