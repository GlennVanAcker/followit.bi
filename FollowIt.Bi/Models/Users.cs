namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Users
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Users()
        {
            CreditChecks = new HashSet<CreditChecks>();
            CreditChecks1 = new HashSet<CreditChecks>();
            NotificationSettings_x_Users = new HashSet<NotificationSettings_x_Users>();
            Organizations_x_Users = new HashSet<Organizations_x_Users>();
            Tasks = new HashSet<Tasks>();
        }

        public long Id { get; set; }

        public string UserIdentifier { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime LastLoginDate { get; set; }

        public string PushOverUserToken { get; set; }

        public string PushOverToken { get; set; }

        public string PushalotToken { get; set; }

        public string Email { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public string CurrentVersion { get; set; }

        public string TeamsAsString { get; set; }

        public string Photo { get; set; }

        public string Description { get; set; }

        public long? PersonId { get; set; }

        public string UserTypeAsString { get; set; }

        public string ShortCode { get; set; }

        public bool Disabled { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CreditChecks> CreditChecks { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CreditChecks> CreditChecks1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NotificationSettings_x_Users> NotificationSettings_x_Users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Organizations_x_Users> Organizations_x_Users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tasks> Tasks { get; set; }
    }
}
