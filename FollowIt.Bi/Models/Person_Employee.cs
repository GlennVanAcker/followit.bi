namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Person_Employee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Person_Employee()
        {
            CreditHistories = new HashSet<CreditHistories>();
            EmployeeCertifications = new HashSet<EmployeeCertifications>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        public string Blog { get; set; }

        public bool? IsMcsdCertified { get; set; }

        public string Website { get; set; }

        public bool NoRunningContract { get; set; }

        public DateTime? NoRunningContractStartDate { get; set; }

        public string CoachUserId { get; set; }

        public bool? IsCoupledToCandidate { get; set; }

        public long? CandidateId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CreditHistories> CreditHistories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EmployeeCertifications> EmployeeCertifications { get; set; }

        public virtual Person Person { get; set; }
    }
}
