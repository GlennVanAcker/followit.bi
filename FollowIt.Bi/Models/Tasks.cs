namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Tasks
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public string Label { get; set; }

        public string Color { get; set; }

        public bool IsCompleted { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public long? UserId { get; set; }

        public virtual Users Users { get; set; }
    }
}
