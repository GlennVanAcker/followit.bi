namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Person")]
    public partial class Person
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Person()
        {
            AssetHistories = new HashSet<AssetHistories>();
            Notes = new HashSet<Notes>();
        }

        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => FirstName + " " + LastName; 

        public DateTime? BirthDate { get; set; }

        public DateTime? ContractStartDate { get; set; }

        public DateTime? ContractEndDate { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public string TwitterAccount { get; set; }

        public string MsAccount { get; set; }

        public string LinkedInAccount { get; set; }

        public string WorkEmail { get; set; }

        public string PersonalEmail { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public long? CompanyId { get; set; }

        public string CellPhone { get; set; }

        public string Title { get; set; }

        public bool OutOfService { get; set; }

        public int Sex { get; set; }

        public string Skype { get; set; }

        public string Nationality { get; set; }

        public string Comment { get; set; }

        public Guid? ProfilePictureIdentifier { get; set; }

        public DateTime? DatRead { get; set; }

        public string UserRead { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AssetHistories> AssetHistories { get; set; }

        public virtual Companies Companies { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Notes> Notes { get; set; }

        public virtual Person_Address Person_Address { get; set; }

        public virtual Person_Employee Person_Employee { get; set; }

        public virtual Person_OfficeStaffEmployees Person_OfficeStaffEmployees { get; set; }
    }
}
