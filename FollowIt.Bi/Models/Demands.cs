namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Demands
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Demands()
        {
            Proposals = new HashSet<Proposals>();
            Publications = new HashSet<Publications>();
        }

        public long Id { get; set; }

        public bool Hot { get; set; }

        public int DemandStatus { get; set; }

        public string DemandNumber { get; set; }

        public int ContractType { get; set; }

        public string Title { get; set; }

        public string PostingTitle { get; set; }

        public DateTime DemandDate { get; set; }

        public string RunTime { get; set; }

        public string Description { get; set; }

        public DateTime? StartDate { get; set; }

        public string MaximumDayprice { get; set; }

        public string ExtraInfo { get; set; }

        public string DemandText { get; set; }

        public string TrackerId { get; set; }

        public long OrganizationId { get; set; }

        public long? SupplierId { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public string TeamsAsString { get; set; }

        public DateTime? ClosingDate { get; set; }

        public Guid? DemandIdentifier { get; set; }

        public DateTime? DatRead { get; set; }

        public string UserRead { get; set; }

        public virtual Organizations Organizations { get; set; }

        public virtual Organizations Organizations1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Proposals> Proposals { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Publications> Publications { get; set; }
    }
}
