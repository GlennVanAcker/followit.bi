namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SubOrganizations
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SubOrganizations()
        {
            CreditChecks = new HashSet<CreditChecks>();
        }

        public long Id { get; set; }

        public string Name { get; set; }

        public string Btw { get; set; }

        public string Iban { get; set; }

        public string JuridicalForm { get; set; }

        public long OrganizationId { get; set; }

        public long AddressId { get; set; }

        public long SigneeId { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public string AccountingCode { get; set; }

        public virtual Addresses Addresses { get; set; }

        public virtual ContactPersons ContactPersons { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CreditChecks> CreditChecks { get; set; }

        public virtual Organizations Organizations { get; set; }
    }
}
