namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class NotificationSettings_x_Users
    {
        public long Id { get; set; }

        public long NotificationSettingId { get; set; }

        public long UserId { get; set; }

        public bool IsDeleted { get; set; }

        public virtual NotificationSettings NotificationSettings { get; set; }

        public virtual Users Users { get; set; }
    }
}
