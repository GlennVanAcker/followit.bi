namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CreditHistories
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CreditHistories()
        {
            CreditHistoryDetails = new HashSet<CreditHistoryDetails>();
        }

        public long Id { get; set; }

        public int Amount { get; set; }

        public string Comment { get; set; }

        public DateTime Date { get; set; }

        public int CreditType { get; set; }

        public long EmployeeId { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public virtual Person_Employee Person_Employee { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CreditHistoryDetails> CreditHistoryDetails { get; set; }
    }
}
