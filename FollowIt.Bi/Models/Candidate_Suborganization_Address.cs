namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Candidate_Suborganization_Address
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        public string Street { get; set; }

        public string City { get; set; }

        public string ZipCode { get; set; }

        public string ExtraInfo { get; set; }

        public double? Lat { get; set; }

        public double? Lon { get; set; }

        public long? CountryId { get; set; }

        public long? RegionId { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public string RegionName { get; set; }

        public string CountryName { get; set; }

        public virtual Candidate_Suborganization Candidate_Suborganization { get; set; }

        public virtual Countries Countries { get; set; }

        public virtual Regions Regions { get; set; }
    }
}
