namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CreditChecks
    {
        public long Id { get; set; }

        public DateTime? RequestDate { get; set; }

        public DateTime? ValidityDate { get; set; }

        public DateTime? ExecuteDate { get; set; }

        public int Status { get; set; }

        public long SubOrganizationId { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public long? UserRequestedId { get; set; }

        public long? UserExecutedId { get; set; }

        public string Comments { get; set; }

        public Guid? CreditCheckGuid { get; set; }

        public virtual SubOrganizations SubOrganizations { get; set; }

        public virtual Users Users { get; set; }

        public virtual Users Users1 { get; set; }
    }
}
