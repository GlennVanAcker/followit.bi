namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AssetSettingFields
    {
        public long Id { get; set; }

        public string FieldName { get; set; }

        public long AssetSettingId { get; set; }

        public bool IsDeleted { get; set; }

        public bool ShowInList { get; set; }

        public bool IsDisabled { get; set; }

        public int SortIndex { get; set; }

        public int DataType { get; set; }

        public bool IsRequired { get; set; }

        public virtual AssetSettings AssetSettings { get; set; }
    }
}
