namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Actions
    {
        public long Id { get; set; }

        public string Message { get; set; }

        public DateTime Date { get; set; }

        public long? OrganizationId { get; set; }

        public long? CandidateId { get; set; }

        public long? ProposalId { get; set; }

        public long ActionTypeId { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public long? ContactPersonId { get; set; }

        public virtual ActionTypes ActionTypes { get; set; }

        public virtual Candidates Candidates { get; set; }

        public virtual Organizations Organizations { get; set; }

        public virtual Proposals Proposals { get; set; }
    }
}
