namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Candidate_Suborganization
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        public string Name { get; set; }

        public string Btw { get; set; }

        public string Iban { get; set; }

        public string JuridicalForm { get; set; }

        public string AccountingCode { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public virtual Candidate_Suborganization_Address Candidate_Suborganization_Address { get; set; }

        public virtual Candidates Candidates { get; set; }
    }
}
