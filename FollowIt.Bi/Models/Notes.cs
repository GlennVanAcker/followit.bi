namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Notes
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

        public DateTime Date { get; set; }

        public long? PersonId { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public long NoteTypeId { get; set; }

        public long? AssetId { get; set; }

        public bool? IsEncrypted { get; set; }

        public virtual Assets Assets { get; set; }

        public virtual NoteTypes NoteTypes { get; set; }

        public virtual Person Person { get; set; }
    }
}
