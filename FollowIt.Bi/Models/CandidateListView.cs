namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CandidateListView")]
    public partial class CandidateListView
    {
        public long? Id { get; set; }

        [Key]
        [Column(Order = 0)]
        public string FirstName { get; set; }

        [Key]
        [Column(Order = 1)]
        public string LastName { get; set; }

        [Key]
        [Column(Order = 2)]
        public string Title { get; set; }

        [Key]
        [Column(Order = 3)]
        public string Phone { get; set; }

        [Key]
        [Column(Order = 4)]
        public string CellPhone { get; set; }

        [Key]
        [Column(Order = 5)]
        public string Email { get; set; }

        [Key]
        [Column(Order = 6)]
        public string RegionName { get; set; }

        [Key]
        [Column(Order = 7)]
        public string City { get; set; }

        [Key]
        [Column(Order = 8)]
        public string Tags { get; set; }

        public bool? IsDeleted { get; set; }

        public bool? MostPlaceable { get; set; }

        public bool? UnderContract { get; set; }

        public bool? Hot { get; set; }

        public bool? DoNotWorkWith { get; set; }

        public bool? Top { get; set; }

        public bool? CanWeEmailCandidate { get; set; }

        public string TeamsAsString { get; set; }
    }
}
