namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DayPrices
    {
        public long Id { get; set; }

        public DateTime Date { get; set; }

        public decimal Price { get; set; }

        public string ExtraInfo { get; set; }

        public long CandidateId { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public decimal PriceOut { get; set; }

        public decimal ActualMargin { get; set; }

        public decimal ActualGrossMargin { get; set; }

        public virtual Candidates Candidates { get; set; }
    }
}
