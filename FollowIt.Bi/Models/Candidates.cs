namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Candidates
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Candidates()
        {
            Actions = new HashSet<Actions>();
            Candidates_x_Tags = new HashSet<Candidates_x_Tags>();
            DayPrices = new HashSet<DayPrices>();
            Proposals = new HashSet<Proposals>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        public bool IsAutoImported { get; set; }

        public bool IsManuallyValidated { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? BirthDate { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string CellPhone { get; set; }

        public int Origin { get; set; }

        public string ExtraOriginInfo { get; set; }

        public int CandidateType { get; set; }

        public string CompanyInfo { get; set; }

        public string Title { get; set; }

        public DateTime? AvailabilityDate { get; set; }

        public int ContactBeforeAvailabilityDate { get; set; }

        public string WantsToWorkIn { get; set; }

        public string ExtraInfo { get; set; }

        public string Cv { get; set; }

        public string HeadLine { get; set; }

        public string Specialities { get; set; }

        public string Summary { get; set; }

        public string LinkedinProfile { get; set; }

        public string LinkedinPhoto { get; set; }

        public bool Hot { get; set; }

        public bool Top { get; set; }

        public bool DoNotWorkWith { get; set; }

        public bool CanWeEmailCandidate { get; set; }

        public string TagString { get; set; }

        public string TrackerId { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public string TeamsAsString { get; set; }

        public bool MostPlaceable { get; set; }

        public bool UnderContract { get; set; }

        public int Sex { get; set; }

        public bool? IsCoupledToFieldstaff { get; set; }

        public long? FieldStaffId { get; set; }

        public string WorkEmail { get; set; }

        public string BirthPlace { get; set; }

        public string Nationality { get; set; }

        public Guid? ProfilePictureIdentifier { get; set; }

        public string AccountingCode { get; set; }

        public DateTime? DatRead { get; set; }

        public string UserRead { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Actions> Actions { get; set; }

        public virtual Addresses Addresses { get; set; }

        public virtual Candidate_Suborganization Candidate_Suborganization { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Candidates_x_Tags> Candidates_x_Tags { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DayPrices> DayPrices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Proposals> Proposals { get; set; }
    }
}
