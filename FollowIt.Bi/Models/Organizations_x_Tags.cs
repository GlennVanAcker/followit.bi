namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Organizations_x_Tags
    {
        public long Id { get; set; }

        public bool Active { get; set; }

        public long OrganizationId { get; set; }

        public long TagId { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public virtual Organizations Organizations { get; set; }

        public virtual Tags Tags { get; set; }
    }
}
