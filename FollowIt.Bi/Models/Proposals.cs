namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Proposals
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Proposals()
        {
            Actions = new HashSet<Actions>();
            Contract_Drafts = new HashSet<Contract_Drafts>();
        }

        public long Id { get; set; }

        public int ProposalStatus { get; set; }

        public decimal DayPrice { get; set; }

        public DateTime? StartDate { get; set; }

        public string RecruiterExtraInfo { get; set; }

        public DateTime? ProposalDate { get; set; }

        public decimal ProposalPrice { get; set; }

        public string ProposalExtraInfo { get; set; }

        public DateTime? InterviewDate { get; set; }

        public string InterviewExtraInfo { get; set; }

        public bool CandidateSignedOut { get; set; }

        public string TrackerId { get; set; }

        public long DemandId { get; set; }

        public long CandidateId { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public long? Organization_Id { get; set; }

        public string TeamsAsString { get; set; }

        public string InterviewTime { get; set; }

        public int SourceAction { get; set; }

        public int ProposalSource { get; set; }

        public bool IsInbound { get; set; }

        public string Comments { get; set; }

        public DateTime? DatRead { get; set; }

        public string UserRead { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Actions> Actions { get; set; }

        public virtual Candidates Candidates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Contract_Drafts> Contract_Drafts { get; set; }

        public virtual Demands Demands { get; set; }

        public virtual Organizations Organizations { get; set; }
    }
}
