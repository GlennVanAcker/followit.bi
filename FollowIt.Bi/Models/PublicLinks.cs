namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PublicLinks
    {
        public long Id { get; set; }

        public string PublicLinkHash { get; set; }

        public int Type { get; set; }

        public long? DocumentId { get; set; }

        public DateTime AvailabilityWindow { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public int AvailabilityWindowOption { get; set; }
    }
}
