namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ContactPersons
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ContactPersons()
        {
            SubOrganizations = new HashSet<SubOrganizations>();
        }

        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Title { get; set; }

        public int Sex { get; set; }

        public string Phone { get; set; }

        public string CellPhone { get; set; }

        public string MailAddress { get; set; }

        public int ContactAllowed { get; set; }

        public string ExtraInfo { get; set; }

        public long OrganizationId { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsActive { get; set; }

        public long ActionId { get; set; }

        public virtual Organizations Organizations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubOrganizations> SubOrganizations { get; set; }
    }
}
