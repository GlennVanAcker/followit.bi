namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EmployeeExams
    {
        public long Id { get; set; }

        public DateTime? DateAchieved { get; set; }

        public bool Achieved { get; set; }

        public long ExamId { get; set; }

        public long EmployeeCertificationId { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public virtual EmployeeCertifications EmployeeCertifications { get; set; }

        public virtual Exams Exams { get; set; }
    }
}
