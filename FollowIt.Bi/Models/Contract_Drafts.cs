namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Contract_Drafts
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Contract_Drafts()
        {
            ResponsibleStaff = new HashSet<ResponsibleStaff>();
        }

        public long Id { get; set; }

        public bool ContractDraftLocked { get; set; }

        public string CandidateFirstName { get; set; }

        public string CandidateLastName { get; set; }

        public DateTime? CandidateBirthDate { get; set; }

        public DateTime? CandidateStartDate { get; set; }

        public string BirthPlace { get; set; }

        public string Nationality { get; set; }

        public int Sex { get; set; }

        public string CandidateStreet { get; set; }

        public string CandidateCity { get; set; }

        public string CandidateZipCode { get; set; }

        public string CandidateExtraInfo { get; set; }

        public long? CandidateCountryId { get; set; }

        public long? CandidateRegionId { get; set; }

        public string CandidateEmail { get; set; }

        public string CandidateNationality { get; set; }

        public string CandidateWorkEmail { get; set; }

        public string CandidatePhone { get; set; }

        public string CandidateCellPhone { get; set; }

        public int CandidateType { get; set; }

        public string CandidateCompanyName { get; set; }

        public string LegalRep { get; set; }

        public string RepJobTitle { get; set; }

        public string CandidateCompanyVat { get; set; }

        public string CandidateCompanyStreet { get; set; }

        public string CandidateCompanyCity { get; set; }

        public string CandidateCompanyZipCode { get; set; }

        public long? CandidateCompanyCountryId { get; set; }

        public long? CandidateCompanyRegionId { get; set; }

        public string CandidateCompanyBankName { get; set; }

        public string CandidateCompanyIban { get; set; }

        public string BIC { get; set; }

        public string CandidateCompanyBankCountry { get; set; }

        public string CandidateTitle { get; set; }

        public int ContractSendMethodCandidate { get; set; }

        public int ContractSendMethodOrganization { get; set; }

        public bool OrganizationIsPartner { get; set; }

        public string OrganizationName { get; set; }

        public string OrganizationPhone { get; set; }

        public string OrganizationEmail { get; set; }

        public string OrganizationBtw { get; set; }

        public string OrganizationIban { get; set; }

        public string OrganizationStreet { get; set; }

        public string OrganizationCity { get; set; }

        public string OrganizationZipCode { get; set; }

        public long? OrganizationCountryId { get; set; }

        public long? OrganizationRegionId { get; set; }

        public string OrganizationExtraInfo { get; set; }

        public string ContractMadeBy { get; set; }

        public string ContactpersonFirstName { get; set; }

        public string ContactpersonLastName { get; set; }

        public string ContactpersonTitle { get; set; }

        public string ContactpersonPhone { get; set; }

        public string ContactpersonCellPhone { get; set; }

        public string ContactpersonMailAddress { get; set; }

        public string ProjectName { get; set; }

        public string JobTitle { get; set; }

        public string JobDescription { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public long HoursPerDay { get; set; }

        public long HoursPerWeek { get; set; }

        public decimal InRate { get; set; }

        public decimal OutRate { get; set; }

        public string ProjectManagerName { get; set; }

        public string ProjectManagerPhone { get; set; }

        public string ProjectManagerEmail { get; set; }

        public string ExtraRemarks { get; set; }

        public int Sector { get; set; }

        public int ContractType { get; set; }

        public int Source { get; set; }

        public string ReferencePo { get; set; }

        public string Technical { get; set; }

        public string Comment { get; set; }

        public string RemarksOrExit { get; set; }

        public string AccountManager { get; set; }

        public long ProjectManagerId { get; set; }

        public long OrganizationId { get; set; }

        public long ProposalId { get; set; }

        public long CandidateId { get; set; }

        public long SubOrganizationId { get; set; }

        public long AccountManagerId { get; set; }

        public string TeamsAsString { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public string CandidateCompanyExtraInfo { get; set; }

        public string CandidateAccountingCode { get; set; }

        public string PlaceOfWork { get; set; }

        public string CompanyTeam { get; set; }

        public long PlaceOfWOrkAddressId { get; set; }

        public bool HrUnlocked { get; set; }

        public int TrialPeriodAmount { get; set; }

        public int TrialPeriodType { get; set; }

        public int NoticePeriodAmount { get; set; }

        public int NoticePeriodType { get; set; }

        public int PayTermAmount { get; set; }

        public int PayTermType { get; set; }

        public string PayTermComment { get; set; }

        public virtual Countries Countries { get; set; }

        public virtual Countries Countries1 { get; set; }

        public virtual Countries Countries2 { get; set; }

        public virtual Proposals Proposals { get; set; }

        public virtual Regions Regions { get; set; }

        public virtual Regions Regions1 { get; set; }

        public virtual Regions Regions2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ResponsibleStaff> ResponsibleStaff { get; set; }
    }
}
