namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Documents_CreditCheck
    {
        public long Id { get; set; }

        public Guid? CreditCheckGuid { get; set; }

        public string FileType { get; set; }

        public string FileName { get; set; }

        public byte[] Blob { get; set; }

        public int Type { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }
    }
}
