namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ResponsibleStaff")]
    public partial class ResponsibleStaff
    {
        public long Id { get; set; }

        public int JobName { get; set; }

        public string UserName { get; set; }

        public int Percentage { get; set; }

        public long ContractDraftId { get; set; }

        public long UserId { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public virtual Contract_Drafts Contract_Drafts { get; set; }
    }
}
