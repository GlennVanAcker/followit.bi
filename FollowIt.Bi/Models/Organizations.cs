namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Organizations
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Organizations()
        {
            Actions = new HashSet<Actions>();
            Addresses = new HashSet<Addresses>();
            ContactPersons = new HashSet<ContactPersons>();
            Demands = new HashSet<Demands>();
            Demands1 = new HashSet<Demands>();
            Organizations_x_Users = new HashSet<Organizations_x_Users>();
            Organizations_x_Tags = new HashSet<Organizations_x_Tags>();
            Proposals = new HashSet<Proposals>();
            SubOrganizations = new HashSet<SubOrganizations>();
        }

        public long Id { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public string TradeName { get; set; }

        public string Email { get; set; }

        public string Website { get; set; }

        public string Fax { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsAutoImported { get; set; }

        public bool IsManuallyValidated { get; set; }

        public int ContactAllowed { get; set; }

        public bool IsProspect { get; set; }

        public bool IsKeyProspect { get; set; }

        public bool IsClient { get; set; }

        public bool IsCompetition { get; set; }

        public bool IsPartner { get; set; }

        public bool IsSupplier { get; set; }

        public int Sector { get; set; }

        public bool WorksWithExternals { get; set; }

        public string GrootteBedrijf { get; set; }

        public string GrootteIt { get; set; }

        public string NumberOfExternals { get; set; }

        public string SalesLead { get; set; }

        public string WhoDecides { get; set; }

        public bool CloseToStation { get; set; }

        public bool EasyParking { get; set; }

        public bool HighLevelOfExpertise { get; set; }

        public string ExtraInfo { get; set; }

        public string TrackerId { get; set; }

        public string LastAction { get; set; }

        public string TeamsAsString { get; set; }

        public DateTime? DatRead { get; set; }

        public string UserRead { get; set; }

        public string CellPhone { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Actions> Actions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Addresses> Addresses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ContactPersons> ContactPersons { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Demands> Demands { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Demands> Demands1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Organizations_x_Users> Organizations_x_Users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Organizations_x_Tags> Organizations_x_Tags { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Proposals> Proposals { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubOrganizations> SubOrganizations { get; set; }
    }
}
