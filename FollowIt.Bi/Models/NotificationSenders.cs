namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class NotificationSenders
    {
        public long Id { get; set; }

        public string FriendlyName { get; set; }

        public int NotificationSenderType { get; set; }

        public long NotificationSettingId { get; set; }

        public bool IsDeleted { get; set; }

        public virtual NotificationSettings NotificationSettings { get; set; }
    }
}
