namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Addresses
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Addresses()
        {
            SubOrganizations = new HashSet<SubOrganizations>();
        }

        public long Id { get; set; }

        public string Type { get; set; }

        public long? OrganizationId { get; set; }

        public DateTime? DatCreated { get; set; }

        public DateTime? DatEdited { get; set; }

        public string UserCreated { get; set; }

        public string UserEdited { get; set; }

        public bool IsDeleted { get; set; }

        public string Street { get; set; }

        public string City { get; set; }

        public string ZipCode { get; set; }

        public bool IsMainAddress { get; set; }

        public long? CountryId { get; set; }

        public long? RegionId { get; set; }

        public string ExtraInfo { get; set; }

        public string ConsultantTrackerId { get; set; }

        public double? Lat { get; set; }

        public double? Lon { get; set; }

        public string RegionName { get; set; }

        public string CountryName { get; set; }

        public virtual Organizations Organizations { get; set; }

        public virtual Countries Countries { get; set; }

        public virtual Regions Regions { get; set; }

        public virtual Candidates Candidates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubOrganizations> SubOrganizations { get; set; }
    }
}
