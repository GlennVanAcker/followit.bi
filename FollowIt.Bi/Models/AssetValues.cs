namespace FollowIt.Bi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AssetValues
    {
        public long Id { get; set; }

        public string FieldName { get; set; }

        public string Value { get; set; }

        public long AssetId { get; set; }

        public long AssetSettingFieldId { get; set; }

        public bool IsDeleted { get; set; }

        public virtual Assets Assets { get; set; }
    }
}
