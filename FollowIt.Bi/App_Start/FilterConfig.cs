﻿using System.Web;
using System.Web.Mvc;

namespace FollowIt.Bi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
