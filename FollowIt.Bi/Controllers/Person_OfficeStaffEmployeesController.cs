﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using FollowIt.Bi.Models;

namespace FollowIt.Bi.Controllers
{
    public class Person_OfficeStaffEmployeesController : Controller
    {
        private FollowItContext db = new FollowItContext();

        // GET: Person_OfficeStaffEmployees
        public ActionResult Index()
        {
            var person_OfficeStaffEmployees = db.Person_OfficeStaffEmployees.Include(p => p.Person);
            return View(person_OfficeStaffEmployees.ToList());
        }

        // GET: Person_OfficeStaffEmployees/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person_OfficeStaffEmployees person_OfficeStaffEmployees = db.Person_OfficeStaffEmployees.Find(id);
            if (person_OfficeStaffEmployees == null)
            {
                return HttpNotFound();
            }
            return View(person_OfficeStaffEmployees);
        }

        // GET: Person_OfficeStaffEmployees/Create
        public ActionResult Create()
        {
            ViewBag.Id = new SelectList(db.Person, "Id", "FirstName");
            return View();
        }

        // POST: Person_OfficeStaffEmployees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,InternalPhone")] Person_OfficeStaffEmployees person_OfficeStaffEmployees)
        {
            if (ModelState.IsValid)
            {
                db.Person_OfficeStaffEmployees.Add(person_OfficeStaffEmployees);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id = new SelectList(db.Person, "Id", "FirstName", person_OfficeStaffEmployees.Id);
            return View(person_OfficeStaffEmployees);
        }

        // GET: Person_OfficeStaffEmployees/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person_OfficeStaffEmployees person_OfficeStaffEmployees = db.Person_OfficeStaffEmployees.Find(id);
            if (person_OfficeStaffEmployees == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id = new SelectList(db.Person, "Id", "FirstName", person_OfficeStaffEmployees.Id);
            return View(person_OfficeStaffEmployees);
        }

        // POST: Person_OfficeStaffEmployees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,InternalPhone")] Person_OfficeStaffEmployees person_OfficeStaffEmployees)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person_OfficeStaffEmployees).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id = new SelectList(db.Person, "Id", "FirstName", person_OfficeStaffEmployees.Id);
            return View(person_OfficeStaffEmployees);
        }

        // GET: Person_OfficeStaffEmployees/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person_OfficeStaffEmployees person_OfficeStaffEmployees = db.Person_OfficeStaffEmployees.Find(id);
            if (person_OfficeStaffEmployees == null)
            {
                return HttpNotFound();
            }
            return View(person_OfficeStaffEmployees);
        }

        // POST: Person_OfficeStaffEmployees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Person_OfficeStaffEmployees person_OfficeStaffEmployees = db.Person_OfficeStaffEmployees.Find(id);
            db.Person_OfficeStaffEmployees.Remove(person_OfficeStaffEmployees);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
