﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using FollowIt.Bi.Models;

namespace FollowIt.Bi.Controllers
{
    public class Person_EmployeeController : Controller
    {
        private FollowItContext db = new FollowItContext();

        // GET: Person_Employee
        public ActionResult Index()
        {
            var person_Employee = db.Person_Employee.Include(p => p.Person);
            return View(person_Employee.ToList());
        }

        // GET: Person_Employee/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person_Employee person_Employee = db.Person_Employee.Find(id);
            if (person_Employee == null)
            {
                return HttpNotFound();
            }
            return View(person_Employee);
        }

        // GET: Person_Employee/Create
        public ActionResult Create()
        {
            ViewBag.Id = new SelectList(db.Person, "Id", "FirstName");
            return View();
        }

        // POST: Person_Employee/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Blog,IsMcsdCertified,Website,NoRunningContract,NoRunningContractStartDate,CoachUserId,IsCoupledToCandidate,CandidateId")] Person_Employee person_Employee)
        {
            if (ModelState.IsValid)
            {
                db.Person_Employee.Add(person_Employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id = new SelectList(db.Person, "Id", "FirstName", person_Employee.Id);
            return View(person_Employee);
        }

        // GET: Person_Employee/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person_Employee person_Employee = db.Person_Employee.Find(id);
            if (person_Employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id = new SelectList(db.Person, "Id", "FirstName", person_Employee.Id);
            return View(person_Employee);
        }

        // POST: Person_Employee/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Blog,IsMcsdCertified,Website,NoRunningContract,NoRunningContractStartDate,CoachUserId,IsCoupledToCandidate,CandidateId")] Person_Employee person_Employee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person_Employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id = new SelectList(db.Person, "Id", "FirstName", person_Employee.Id);
            return View(person_Employee);
        }

        // GET: Person_Employee/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person_Employee person_Employee = db.Person_Employee.Find(id);
            if (person_Employee == null)
            {
                return HttpNotFound();
            }
            return View(person_Employee);
        }

        // POST: Person_Employee/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Person_Employee person_Employee = db.Person_Employee.Find(id);
            db.Person_Employee.Remove(person_Employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
