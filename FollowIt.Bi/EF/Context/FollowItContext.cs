namespace FollowIt.Bi.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class FollowItContext : DbContext
    {
        public FollowItContext()
            : base("name=FollowItContext1")
        {
        }

        public virtual DbSet<Actions> Actions { get; set; }
        public virtual DbSet<ActionTypes> ActionTypes { get; set; }
        public virtual DbSet<Addresses> Addresses { get; set; }
        public virtual DbSet<AssetHistories> AssetHistories { get; set; }
        public virtual DbSet<Assets> Assets { get; set; }
        public virtual DbSet<AssetSettingFields> AssetSettingFields { get; set; }
        public virtual DbSet<AssetSettings> AssetSettings { get; set; }
        public virtual DbSet<AssetValues> AssetValues { get; set; }
        public virtual DbSet<Candidate_Suborganization> Candidate_Suborganization { get; set; }
        public virtual DbSet<Candidate_Suborganization_Address> Candidate_Suborganization_Address { get; set; }
        public virtual DbSet<Candidates> Candidates { get; set; }
        public virtual DbSet<Candidates_x_Tags> Candidates_x_Tags { get; set; }
        public virtual DbSet<Certifications> Certifications { get; set; }
        public virtual DbSet<Companies> Companies { get; set; }
        public virtual DbSet<ContactPersons> ContactPersons { get; set; }
        public virtual DbSet<Contract_Drafts> Contract_Drafts { get; set; }
        public virtual DbSet<Countries> Countries { get; set; }
        public virtual DbSet<CreditChecks> CreditChecks { get; set; }
        public virtual DbSet<CreditHistories> CreditHistories { get; set; }
        public virtual DbSet<CreditHistoryDetails> CreditHistoryDetails { get; set; }
        public virtual DbSet<DayPrices> DayPrices { get; set; }
        public virtual DbSet<Demands> Demands { get; set; }
        public virtual DbSet<Documents> Documents { get; set; }
        public virtual DbSet<Documents_CreditCheck> Documents_CreditCheck { get; set; }
        public virtual DbSet<Documents_Demands> Documents_Demands { get; set; }
        public virtual DbSet<Documents_ProfilePictures> Documents_ProfilePictures { get; set; }
        public virtual DbSet<EmployeeCertifications> EmployeeCertifications { get; set; }
        public virtual DbSet<EmployeeExams> EmployeeExams { get; set; }
        public virtual DbSet<Exams> Exams { get; set; }
        public virtual DbSet<Notes> Notes { get; set; }
        public virtual DbSet<NoteTypes> NoteTypes { get; set; }
        public virtual DbSet<Notifications> Notifications { get; set; }
        public virtual DbSet<NotificationSenders> NotificationSenders { get; set; }
        public virtual DbSet<NotificationSettings> NotificationSettings { get; set; }
        public virtual DbSet<NotificationSettings_x_Users> NotificationSettings_x_Users { get; set; }
        public virtual DbSet<Organizations> Organizations { get; set; }
        public virtual DbSet<Organizations_x_Tags> Organizations_x_Tags { get; set; }
        public virtual DbSet<Organizations_x_Users> Organizations_x_Users { get; set; }
        public virtual DbSet<Person> Person { get; set; }
        public virtual DbSet<Person_Address> Person_Address { get; set; }
        public virtual DbSet<Person_Employee> Person_Employee { get; set; }
        public virtual DbSet<Person_OfficeStaffEmployees> Person_OfficeStaffEmployees { get; set; }
        public virtual DbSet<Proposals> Proposals { get; set; }
        public virtual DbSet<Publications> Publications { get; set; }
        public virtual DbSet<PublicLinks> PublicLinks { get; set; }
        public virtual DbSet<Regions> Regions { get; set; }
        public virtual DbSet<ResponsibleStaff> ResponsibleStaff { get; set; }
        public virtual DbSet<SubOrganizations> SubOrganizations { get; set; }
        public virtual DbSet<TagCategories> TagCategories { get; set; }
        public virtual DbSet<Tags> Tags { get; set; }
        public virtual DbSet<Tasks> Tasks { get; set; }
        public virtual DbSet<Teams> Teams { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<CandidateListView> CandidateListView { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActionTypes>()
                .HasMany(e => e.Actions)
                .WithRequired(e => e.ActionTypes)
                .HasForeignKey(e => e.ActionTypeId);

            modelBuilder.Entity<Addresses>()
                .HasOptional(e => e.Candidates)
                .WithRequired(e => e.Addresses);

            modelBuilder.Entity<Addresses>()
                .HasMany(e => e.SubOrganizations)
                .WithRequired(e => e.Addresses)
                .HasForeignKey(e => e.AddressId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Assets>()
                .HasMany(e => e.AssetHistories)
                .WithRequired(e => e.Assets)
                .HasForeignKey(e => e.AssetId);

            modelBuilder.Entity<Assets>()
                .HasMany(e => e.AssetValues)
                .WithRequired(e => e.Assets)
                .HasForeignKey(e => e.AssetId);

            modelBuilder.Entity<Assets>()
                .HasMany(e => e.Notes)
                .WithOptional(e => e.Assets)
                .HasForeignKey(e => e.AssetId);

            modelBuilder.Entity<AssetSettings>()
                .HasMany(e => e.Assets)
                .WithRequired(e => e.AssetSettings)
                .HasForeignKey(e => e.AssetSettingId);

            modelBuilder.Entity<AssetSettings>()
                .HasMany(e => e.AssetSettingFields)
                .WithRequired(e => e.AssetSettings)
                .HasForeignKey(e => e.AssetSettingId);

            modelBuilder.Entity<Candidate_Suborganization>()
                .HasOptional(e => e.Candidate_Suborganization_Address)
                .WithRequired(e => e.Candidate_Suborganization);

            modelBuilder.Entity<Candidates>()
                .HasMany(e => e.Actions)
                .WithOptional(e => e.Candidates)
                .HasForeignKey(e => e.CandidateId);

            modelBuilder.Entity<Candidates>()
                .HasOptional(e => e.Candidate_Suborganization)
                .WithRequired(e => e.Candidates);

            modelBuilder.Entity<Candidates>()
                .HasMany(e => e.Candidates_x_Tags)
                .WithRequired(e => e.Candidates)
                .HasForeignKey(e => e.CandidateId);

            modelBuilder.Entity<Candidates>()
                .HasMany(e => e.DayPrices)
                .WithRequired(e => e.Candidates)
                .HasForeignKey(e => e.CandidateId);

            modelBuilder.Entity<Candidates>()
                .HasMany(e => e.Proposals)
                .WithRequired(e => e.Candidates)
                .HasForeignKey(e => e.CandidateId);

            modelBuilder.Entity<Certifications>()
                .HasMany(e => e.EmployeeCertifications)
                .WithRequired(e => e.Certifications)
                .HasForeignKey(e => e.CertificationId);

            modelBuilder.Entity<Certifications>()
                .HasMany(e => e.Exams)
                .WithRequired(e => e.Certifications)
                .HasForeignKey(e => e.CertificationId);

            modelBuilder.Entity<Companies>()
                .HasMany(e => e.Assets)
                .WithOptional(e => e.Companies)
                .HasForeignKey(e => e.CompanyId);

            modelBuilder.Entity<Companies>()
                .HasMany(e => e.Person)
                .WithOptional(e => e.Companies)
                .HasForeignKey(e => e.CompanyId);

            modelBuilder.Entity<Companies>()
                .HasMany(e => e.Teams)
                .WithRequired(e => e.Companies)
                .HasForeignKey(e => e.CompanyId);

            modelBuilder.Entity<ContactPersons>()
                .HasMany(e => e.SubOrganizations)
                .WithRequired(e => e.ContactPersons)
                .HasForeignKey(e => e.SigneeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Contract_Drafts>()
                .HasMany(e => e.ResponsibleStaff)
                .WithRequired(e => e.Contract_Drafts)
                .HasForeignKey(e => e.ContractDraftId);

            modelBuilder.Entity<Countries>()
                .HasMany(e => e.Addresses)
                .WithOptional(e => e.Countries)
                .HasForeignKey(e => e.CountryId);

            modelBuilder.Entity<Countries>()
                .HasMany(e => e.Candidate_Suborganization_Address)
                .WithOptional(e => e.Countries)
                .HasForeignKey(e => e.CountryId);

            modelBuilder.Entity<Countries>()
                .HasMany(e => e.Contract_Drafts)
                .WithOptional(e => e.Countries)
                .HasForeignKey(e => e.CandidateCompanyCountryId);

            modelBuilder.Entity<Countries>()
                .HasMany(e => e.Contract_Drafts1)
                .WithOptional(e => e.Countries1)
                .HasForeignKey(e => e.CandidateCountryId);

            modelBuilder.Entity<Countries>()
                .HasMany(e => e.Contract_Drafts2)
                .WithOptional(e => e.Countries2)
                .HasForeignKey(e => e.OrganizationCountryId);

            modelBuilder.Entity<Countries>()
                .HasMany(e => e.Person_Address)
                .WithOptional(e => e.Countries)
                .HasForeignKey(e => e.CountryId);

            modelBuilder.Entity<Countries>()
                .HasMany(e => e.Regions)
                .WithRequired(e => e.Countries)
                .HasForeignKey(e => e.CountryId);

            modelBuilder.Entity<CreditHistories>()
                .HasMany(e => e.CreditHistoryDetails)
                .WithRequired(e => e.CreditHistories)
                .HasForeignKey(e => e.CreditHistoryRecordId);

            modelBuilder.Entity<Demands>()
                .HasMany(e => e.Proposals)
                .WithRequired(e => e.Demands)
                .HasForeignKey(e => e.DemandId);

            modelBuilder.Entity<Demands>()
                .HasMany(e => e.Publications)
                .WithRequired(e => e.Demands)
                .HasForeignKey(e => e.DemandId);

            modelBuilder.Entity<EmployeeCertifications>()
                .HasMany(e => e.EmployeeExams)
                .WithRequired(e => e.EmployeeCertifications)
                .HasForeignKey(e => e.EmployeeCertificationId);

            modelBuilder.Entity<Exams>()
                .HasMany(e => e.EmployeeExams)
                .WithRequired(e => e.Exams)
                .HasForeignKey(e => e.ExamId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<NoteTypes>()
                .HasMany(e => e.Notes)
                .WithRequired(e => e.NoteTypes)
                .HasForeignKey(e => e.NoteTypeId);

            modelBuilder.Entity<NotificationSettings>()
                .HasMany(e => e.NotificationSenders)
                .WithRequired(e => e.NotificationSettings)
                .HasForeignKey(e => e.NotificationSettingId);

            modelBuilder.Entity<NotificationSettings>()
                .HasMany(e => e.NotificationSettings_x_Users)
                .WithRequired(e => e.NotificationSettings)
                .HasForeignKey(e => e.NotificationSettingId);

            modelBuilder.Entity<Organizations>()
                .HasMany(e => e.Actions)
                .WithOptional(e => e.Organizations)
                .HasForeignKey(e => e.OrganizationId);

            modelBuilder.Entity<Organizations>()
                .HasMany(e => e.Addresses)
                .WithOptional(e => e.Organizations)
                .HasForeignKey(e => e.OrganizationId);

            modelBuilder.Entity<Organizations>()
                .HasMany(e => e.ContactPersons)
                .WithRequired(e => e.Organizations)
                .HasForeignKey(e => e.OrganizationId);

            modelBuilder.Entity<Organizations>()
                .HasMany(e => e.Demands)
                .WithRequired(e => e.Organizations)
                .HasForeignKey(e => e.OrganizationId);

            modelBuilder.Entity<Organizations>()
                .HasMany(e => e.Demands1)
                .WithOptional(e => e.Organizations1)
                .HasForeignKey(e => e.SupplierId);

            modelBuilder.Entity<Organizations>()
                .HasMany(e => e.Organizations_x_Users)
                .WithRequired(e => e.Organizations)
                .HasForeignKey(e => e.OrganizationId);

            modelBuilder.Entity<Organizations>()
                .HasMany(e => e.Organizations_x_Tags)
                .WithRequired(e => e.Organizations)
                .HasForeignKey(e => e.OrganizationId);

            modelBuilder.Entity<Organizations>()
                .HasMany(e => e.Proposals)
                .WithOptional(e => e.Organizations)
                .HasForeignKey(e => e.Organization_Id);

            modelBuilder.Entity<Organizations>()
                .HasMany(e => e.SubOrganizations)
                .WithRequired(e => e.Organizations)
                .HasForeignKey(e => e.OrganizationId);

            modelBuilder.Entity<Person>()
                .HasOptional(e => e.Person_Address)
                .WithRequired(e => e.Person);

            modelBuilder.Entity<Person>()
                .HasOptional(e => e.Person_Employee)
                .WithRequired(e => e.Person);

            modelBuilder.Entity<Person>()
                .HasOptional(e => e.Person_OfficeStaffEmployees)
                .WithRequired(e => e.Person);

            modelBuilder.Entity<Person_Employee>()
                .HasMany(e => e.CreditHistories)
                .WithRequired(e => e.Person_Employee)
                .HasForeignKey(e => e.EmployeeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Person_Employee>()
                .HasMany(e => e.EmployeeCertifications)
                .WithRequired(e => e.Person_Employee)
                .HasForeignKey(e => e.EmployeeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Proposals>()
                .HasMany(e => e.Actions)
                .WithOptional(e => e.Proposals)
                .HasForeignKey(e => e.ProposalId);

            modelBuilder.Entity<Proposals>()
                .HasMany(e => e.Contract_Drafts)
                .WithRequired(e => e.Proposals)
                .HasForeignKey(e => e.ProposalId);

            modelBuilder.Entity<Regions>()
                .HasMany(e => e.Addresses)
                .WithOptional(e => e.Regions)
                .HasForeignKey(e => e.RegionId);

            modelBuilder.Entity<Regions>()
                .HasMany(e => e.Candidate_Suborganization_Address)
                .WithOptional(e => e.Regions)
                .HasForeignKey(e => e.RegionId);

            modelBuilder.Entity<Regions>()
                .HasMany(e => e.Contract_Drafts)
                .WithOptional(e => e.Regions)
                .HasForeignKey(e => e.CandidateCompanyRegionId);

            modelBuilder.Entity<Regions>()
                .HasMany(e => e.Contract_Drafts1)
                .WithOptional(e => e.Regions1)
                .HasForeignKey(e => e.CandidateRegionId);

            modelBuilder.Entity<Regions>()
                .HasMany(e => e.Contract_Drafts2)
                .WithOptional(e => e.Regions2)
                .HasForeignKey(e => e.OrganizationRegionId);

            modelBuilder.Entity<Regions>()
                .HasMany(e => e.Person_Address)
                .WithOptional(e => e.Regions)
                .HasForeignKey(e => e.RegionId);

            modelBuilder.Entity<SubOrganizations>()
                .HasMany(e => e.CreditChecks)
                .WithRequired(e => e.SubOrganizations)
                .HasForeignKey(e => e.SubOrganizationId);

            modelBuilder.Entity<TagCategories>()
                .HasMany(e => e.Tags)
                .WithRequired(e => e.TagCategories)
                .HasForeignKey(e => e.TagCategoryId);

            modelBuilder.Entity<Tags>()
                .HasMany(e => e.Candidates_x_Tags)
                .WithRequired(e => e.Tags)
                .HasForeignKey(e => e.TagId);

            modelBuilder.Entity<Tags>()
                .HasMany(e => e.Organizations_x_Tags)
                .WithRequired(e => e.Tags)
                .HasForeignKey(e => e.TagId);

            modelBuilder.Entity<Users>()
                .HasMany(e => e.CreditChecks)
                .WithOptional(e => e.Users)
                .HasForeignKey(e => e.UserExecutedId);

            modelBuilder.Entity<Users>()
                .HasMany(e => e.CreditChecks1)
                .WithOptional(e => e.Users1)
                .HasForeignKey(e => e.UserRequestedId);

            modelBuilder.Entity<Users>()
                .HasMany(e => e.NotificationSettings_x_Users)
                .WithRequired(e => e.Users)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<Users>()
                .HasMany(e => e.Organizations_x_Users)
                .WithRequired(e => e.Users)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<Users>()
                .HasMany(e => e.Tasks)
                .WithOptional(e => e.Users)
                .HasForeignKey(e => e.UserId);
        }
    }
}
